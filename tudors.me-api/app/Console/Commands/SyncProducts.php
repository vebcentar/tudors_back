<?php

namespace App\Console\Commands;

use App\Models\Products\ProductSizes;
use App\Models\Products\ProductsModel;
use App\Models\Products\ProductStockModel;
use Illuminate\Console\Command;
use function App\Http\Controllers\Products\array_except;

class SyncProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync with Abacus DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json = file_get_contents('http://android.pos4.me:8125/Benetton.aspx/?u=b2b&p=!b2b!&t=b2bLagerListaOsobine&idm=3&idp=');
        $json2 = file_get_contents('http://android.pos4.me:8125/Benetton.aspx/?u=b2b&p=!b2b!&t=b2bLagerListaOsobine&idm=4&idp=');
        $obj = json_decode($json);
        $obj2 = json_decode($json2);

        $obj_merged = (object)array_merge(
            (array)$obj, (array)$obj2);


        function arr_exx($array, $keys)
        {
            return array_diff_key($array, array_flip((array)$keys));
        }

        $sifre = [];
        $velicine = [];

        foreach ($obj_merged as $o) {
            array_push($sifre, arr_exx((array)$o, ["naziv", "size", "color", "cijena", "kolicina"])["sifra"]);
            array_push($velicine, arr_exx((array)$o, ["sifra", "naziv", "color", "cijena", "kolicina"])["size"]);
        }

        $products = ProductsModel::select("id")->whereNotIn("product_code", array_values(array_unique($sifre)))->get();

        $productsArray = [];
        foreach ($products as $p) {
            array_push($productsArray, $p->id);
        }

        try {
            ProductStockModel::whereIn("product_id", $productsArray)->delete();
        } catch (\Exception $e) {
        }


        $arr = [];
//        $unique = self::unique_key($obj, "sifra");
        foreach ($obj_merged as $o) {
            $code = strlen($o->sifra) === 9 ? $o->sifra : substr($o->sifra, 0, -1);
            $product = ProductsModel::where("product_code", "like", "%" . $o->sifra . "%")
                ->where("color", "like", $o->color)
                ->first();
            if (!$product) {
                $product = ProductsModel::create(["product_code" => $o->sifra, "color" => $o->color, "category_id" => 290, "name" => $o->naziv, "price" => $o->cijena]);
                $s = ProductSizes::firstOrCreate(["name" => $o->size]);
                ProductStockModel::updateOrCreate(["product_id" => $product->id, "product_size_id" => $s->id], ["amount" => $o->kolicina]);
                continue;
            } else {
                $product->price = $o->cijena;
                $product->save();
                array_push($arr, $product->product_code);
                $s = ProductSizes::firstOrCreate(["name" => $o->size]);
                ProductStockModel::updateOrCreate(["product_id" => $product->id, "product_size_id" => $s->id], ["amount" => $o->kolicina]);
            }
        }


        return response()->json(["success" => true, "message" => "DB synchronized successfully"]);

    }
}
