<?php

namespace App\Http\Controllers\Orders;

use App\Http\Controllers\Controller;
use App\Models\Orders\OrdersItemsModel;
use App\Models\Orders\OrdersModel;
use App\Models\Products\ProductsModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App;
use File;

class OrdersController extends Controller
{
    use App\Traits\Mail\MailingTraits;

    public static function addOrder(Request $request)
    {
        $rules = [
            "name" => "required",
            "email" => "required|email",
            "phone" => "required",
            "address" => "required|string",
            "city" => "required|string",
            "customer_id" => "required|int",
            "order_items" => "required",
            "payment_type" => "required|int"
        ];
        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }
        $order = OrdersModel::create($request->except("order_items"));
        if (!$order) {
            return response()->json(["success" => false, "message" => "Something went wrong while creating order"]);
        }
        $orderItems = json_decode($request->get("order_items"));
        $data = [];
        $product_items = [];
        foreach ($orderItems as $o) {
            $product = ProductsModel::find($o->product_id);
            $product->save();
            $productPrice = (double)$product->price;
            $discount = 0;
            if ($product->discount_id != 0) {
                $discount = App\Models\Discounts\DiscountsModel::find($product->discount_id);
                if ($discount) {
                    $productPrice = ((double)$product->price * 100) / (double)$discount->discount;
                }
            }

            array_push($data,
                [
                    "product_id" => $o->product_id,
                    "order_id" => $order->id,
                    "product_code" => $o->product_code,
                    "product_color" => $o->product_color,
                    "size_id" => $o->size_id,
                    "amount" => $o->amount,
                    "discount" => $discount->discount
                ]
            );

            array_push($product_items,
                [
                    "name" => $product->name,
                    "price" => $productPrice,
                    "price_total" => $productPrice * (double)$o->amount
                ]
            );
        }

        $total = 0;

        foreach ($product_items as $p) {
            $total += $p["price_total"];
        }

        $orderItemsInsert = OrdersItemsModel::insert($data);
        if (!$orderItemsInsert) {
            return response()->json(["success" => false, "message" => "Something went wrong while creating order items"]);
        }
        $order["items"] = $product_items;
        if ($request->get("payment_type") == 1) {
            $or = self::renderPDF($order->id);
            if (!$or) {
                return response()->json(["success" => false, "message" => "Something went wrong"]);
            }
        }
        return response()->json(["success" => true, "order" => $order]);
    }

    public static function getAllOrders(Request $request)
    {
        $offset = $request->get("offset") ? $request->get("offset") : 0;
        $orders = OrdersModel::orderBy("id", "desc")->limit(20)->offset($offset)->get();
        if (!$orders) {
            return response()->json(["success" => false, "message" => "Orders not found"]);
        }
        $order_total = 0;
        foreach ($orders as $order) {
            $items = OrdersItemsModel::where("order_id", "=", $order->id)
                ->leftJoinSub("select name,price,id from flex_product", "product", "product.id", "=", "flex_orders_items.product_id")
                ->limit(20)
                ->get();


            if ($order["payment_type"] == 1) {
                $order["payment_type"] = "Plaćanje gotovinski prilikom preuzimanja";
            } elseif ($order["payment_type"] == 2) {
                $order["payment_type"] = "Plaćanje kreditnom karticom";
            }


            foreach ($items as $item) {
                $discount = $item["discount"];
                $price = (double)$item["price"];
                if ($discount > 0) {
                    $price = $price - ($price * $discount / 100);
                    $item["price"] = $price;
                }
                $item["total"] = round((double)$item["amount"] * $price, 2);
                $order_total += $item["total"];
            }
            $order["order_total"] = round($order_total, 2);
            $order["items"] = $items;
            $order_total = 0;
        }

        return response()->json(["success" => true, "orders" => $orders, "offset" => $offset]);
    }

    public static function getOrder(Request $request)
    {
        $rules = [
            "id" => "required",
        ];
        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }

        $order = OrdersModel::find($request->get("id"));

        if (!$order) {
            return response()->json(["success" => false, "message" => "Order not found"]);
        }

        $items = OrdersItemsModel::where("order_id", "=", $order->id)
            ->leftJoinSub("select name,price,product_code,id from flex_product", "product", "product.id", "=", "flex_orders_items.product_id")
            ->get();

        foreach ($items as $item) {
            if ($item["unit"] == 0) {
                $item["unit"] = "kg";
            } else {
                $item["unit"] = "komad";
            }
            $discount = $item["discount"];
            $price = (double)$item["price"];
            if ($discount > 0) {
                $price = $price - ($price * $discount / 100);
                $item["price"] = $price;
            }
            $item["total"] = round((double)$item["amount"] * $price, 2);
        }
        if ($order["payment_type"] == 1) {
            $order["payment_type"] = "Plaćanje gotovinski prilikom preuzimanja";
        } elseif ($order["payment_type"] == 2) {
            $order["payment_type"] = "Plaćanje kreditnom karticom";
        }
        if ($order["type"] == 0) {
            $order["type"] = "Fizičko lice";
        } else {
            $order["type"] = "Pravno lice";
        }
        $order["items"] = $items;
        return response()->json(["success" => true, "order" => $order]);
    }

    public static function updateOrder(Request $request)
    {
        $rules = [
            "session" => "required|string",
            "response_code" => "required|string",
            "response_text" => "required|string"
        ];
        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }

        $order = OrdersModel::where("session", "=", $request->get("session"))->update($request->all());

        if (!$order) {
            return response()->json(["success" => false, "message" => "Order not found"]);
        }

        return response()->json(["success" => true, "message" => "Order updated successfully"]);
    }

    public static function getOrderBySession(Request $request)
    {
        $rules = [
            "session" => "required|string",
        ];
        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }
        $order = OrdersModel::where("session", "=", $request->get("session"))->first();
        if (!$order) {
            return response()->json(["success" => false, "message" => "Order not found"]);
        }

        $res_code = $order->response_code;
        $res_text = $order->response_text;

        try {
            if ($res_code != "00") {
                $order->delete();
            } else {
                self::renderPDF($order->id);
            }
        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e]);
        }

        return response()->json(["success" => true, "response_code" => $res_code, "response_text" => $res_text]);
    }

    public static function deleteOrder(Request $request)
    {
        $rules = [
            "id" => "required",
        ];
        $validation = ValidateHttpRequest($rules, $request);
        if ($validation) {
            return response()->json(array_merge($validation));
        }
        $order = OrdersModel::find($request->get("id"));
        if (!$order) {
            return response()->json(["success" => false, "Order not found"]);
        }
        try {
            $order->delete();
            return response()->json(["success" => true, "message" => "Order deleted"]);
        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e]);
        }
    }

    public static function renderPDF($id)
    {
        $order = OrdersModel::find($id);
        if (!$order) {
            return response()->json(["success" => false, "message" => "Order not found"]);
        }
        $items = OrdersItemsModel::where("order_id", "=", $order->id)
            ->leftJoinSub("select name,price,product_code,id from flex_product", "product", "product.id", "=", "flex_orders_items.product_id")
            ->leftJoinSub("select name as sizeName, id from flex_product_sizes", "size", "size.id", "=", "flex_orders_items.size_id")
            ->get();
        foreach ($items as $item) {
            $discount = $item["discount"];
            $price = (double)$item["price"];
            if ($discount > 0) {
                $price = $price - ($price * $discount / 100);
                $item["price"] = $price;
            }
            $item["total"] = round((double)$item["amount"] * $price, 2);
        }
        if ($order["payment_type"] == 1) {
            $order["payment_type"] = "Plaćanje gotovinski prilikom preuzimanja";
        } elseif ($order["payment_type"] == 2) {
            $order["payment_type"] = "Uplata na tekući račun";
        } elseif ($order["payment_type"] == 3) {
            $order["payment_type"] = "Plaćanje kreditnom karticom prilikom preuzimanja";
        }
        if ($order["type"] == 0) {
            $order["type"] = "Fizičko lice";
        } else {
            $order["type"] = "Pravno lice";
        }
        $order["items"] = $items;

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('orderPDF', ["order" => $order]);
        $output = $pdf->output();
        $path = "orders/" . Carbon::now()->year . "/" . Carbon::now()->format('m') . "/";
        $fileID = "order-" . Carbon::now()->format('d-m-Y') . "-(#" . $order->id . ").pdf";
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0755, true, true);
        }
        File::put(public_path($path . $fileID), $output);
        $order["pdf"] = $path . $fileID;
        return self::orderPDF($order);
    }

    public static function designPDF()
    {
        $order = OrdersModel::find(2);

        if (!$order) {
            return response()->json(["success" => false, "Order not found"]);
        }

        $items = OrdersItemsModel::where("order_id", "=", $order->id)
            ->leftJoinSub("select name,price,product_code,id from flex_product", "product", "product.id", "=", "flex_orders_items.product_id")
            ->get();

        foreach ($items as $item) {
            $item["total"] = round((double)$item["amount"] * (double)$item["price"], 2);
        }

        if ($order["payment_type"] == 1) {
            $order["payment_type"] = "Plaćanje gotovinski prilikom preuzimanja";
        } elseif ($order["payment_type"] == 2) {
            $order["payment_type"] = "Plaćanje kreditnom karticom";
        }
        if ($order["type"] == 0) {
            $order["type"] = "Fizičko lice";
        } else {
            $order["type"] = "Pravno lice";
        }
        $order["items"] = $items;

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('orderPDF', ["order" => $order]);
        $output = $pdf->output();
        $path = "orders/" . Carbon::now()->year . "/" . Carbon::now()->format('m') . "/";
        $fileID = "order-" . Carbon::now()->format('d-m-Y') . "-(#" . $order->id . ").pdf";
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0755, true, true);
        }
        File::put(public_path($path . $fileID), $output);
        $order["pdf"] = $path . $fileID;
        return $pdf->stream();
    }
}
