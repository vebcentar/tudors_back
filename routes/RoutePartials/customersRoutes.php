<?php

Route::group(
    ['prefix' => 'customers/'],
    function () {
        Route::group(
            ['middleware' => 'auth:api'],
            function () {
                Route::post('/register', 'Customers\CustomersController@register');
            });
        Route::post('/register', 'Customers\CustomersController@register');
        Route::post('/login', 'Customers\CustomersController@login');

    });