<?php
Route::group(
    ['prefix' => 'products/'],
    function () {
        Route::group(
            ['middleware' => 'auth:api'],
            function () {
                Route::post('/addProduct', 'Products\ProductsController@addProduct');
                Route::post('/deleteProduct', 'Products\ProductsController@deleteProduct');
                Route::post('/updateProduct', 'Products\ProductsController@updateProduct');
                Route::post('/addImages', 'Products\ProductsController@addImages');
                Route::post('/deleteImage', 'Products\ProductsController@deleteImage');
                Route::post('/sortProducts', 'Products\ProductsController@sortProducts');
                Route::post('/sortImages', 'Products\ProductsController@sortImages');
                Route::post('/syncProducts', 'Products\ProductsController@syncProducts');
                Route::post('/toggleSpecialOffer', 'Products\ProductsController@toggleSpecialOffer');
            });
        Route::post('/addProductCategory', 'Products\ProductsController@addProductCategory');
        Route::post('/getAllProductsForSorting', 'Products\ProductsController@getAllProductsForSorting');
        Route::post('/getAllProducts', 'Products\ProductsController@getAllProducts');
		Route::post('/getAllFilterProducts','Products\ProductsController@getAllFilterProducts');
        Route::post('/getAllSizes', 'Products\ProductsController@getAllSizes');
		Route::post('/getFilterSizes', 'Products\ProductsController@getFilterSizes');
		Route::post('/getFilterPrices', 'Products\ProductsController@getFilterPrices');
		Route::post('/getAllColors', 'Products\ProductsController@getAllColors');
		Route::post('/getFilterColors', 'Products\ProductsController@getFilterColors');
        Route::post('/specialOffers', 'Products\ProductsController@specialOffers');
        Route::post('/getProduct', 'Products\ProductsController@getProduct');
        Route::get('/getAbacus', 'Products\ProductsController@getAbacus');
		Route::get('/getNames', 'Products\ProductsController@getNames');
		Route::get('/getNames2', 'Products\ProductsController@getNames2');
    });
