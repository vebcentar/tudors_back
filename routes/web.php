<?php


Route::get('/testName', 'Products\ProductsController@testName');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/getNewProducts', 'Products\ProductsController@getNewProducts');
Route::get('/updateCategories', 'Products\ProductsController@updateCategories');
Route::get('/syncProducts', 'Products\ProductsController@syncProducts');
Route::get('/confirm/{token}', 'Customers\CustomersController@confirmUser');

//Route::get('/designPDF', 'Orders\OrdersController@designPDF');


Route::get('/test', 'Orders\OrdersController@renderPDF');

Route::get('{any}', function () {
    return view('welcome');
});

Route::get('/{any}', function () {
    return view('welcome');
})->where('any', '.*');


